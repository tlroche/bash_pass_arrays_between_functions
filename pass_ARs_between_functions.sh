#!/usr/bin/env bash

### USAGE: /path/to/pass_arrays_between_functions.sh
### Path can be FQ or relative.

### Illustrating how to pass `bash` array representations (ARs) between `bash` functions,
### in order to avoid problems encountered when trying to pass/return complex data in `bash`.
### Note:
### * The examples are not presented as the best ways to solve these problems,
###   only to show that  one *can* interface between `bash` functions and their callers
###   with what are effectively arrays, rather than the usual simple strings.
### * This was initially inspired by my https://stackoverflow.com/a/39502863/915044
###   (which has my acknowledgements to 2nd-order inspirations)
###   then further encouraged by difficulties with https://bitbucket.org/tlroche/backup_utils/

### Following attempts to observe Google Shell Style Guide currently @ https://google.github.io/styleguide/shell.xml

### TODO:
### * (always) run `shellcheck` pre-commit!
### *

### Copyright (C) 2019 Tom Roche <Tom_Roche@pobox.com>
### This work is licensed under the Creative Commons Attribution 4.0 International License.
### To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
### Latest version of this file (or pointer to same) should be available @
### http://bitbucket.org/tlroche/bash_pass_arrays_between_functions/src/master/pass_arrays_between_functions.sh

### ------------------------------------------------------------------
### constants (and very slight init code)
### ------------------------------------------------------------------

## Filesystem constants
## Ordinarily would declare all my `Messaging constants` here,
## but then `source`ing overrides their constants. (TODO: find cleaner way)
## Don't declare following `-r`: more below.

# FQ path to our $(pwd)
declare THIS_DIR="$(readlink -f "$(dirname "${BASH_SOURCE}")")"

## Dependencies

declare -r TRANSFORM_FUNCTIONS_FN='functions_for_lines_to-from_ARs.sh'
declare -r TRANSFORM_FUNCTIONS_FP="${THIS_DIR}/${TRANSFORM_FUNCTIONS_FN}"

### -----------------------------------------------------------------------
### code
### -----------------------------------------------------------------------

### -----------------------------------------------------------------------
### init code (plus more constants)
### -----------------------------------------------------------------------

source "${TRANSFORM_FUNCTIONS_FP}"
## TODO: test return value, errorcode, etc

## Messaging constants
## Don't declare following `-r`: same names are used in other files in this project.
## (TODO: find how to reconcile both read-only declarations, which is my intent.)

declare THIS_FP="${BASH_SOURCE}" # works when either executing or `source`ing
declare THIS_FN="$(basename "${THIS_FP}")"
# declare THIS_DIR above
declare MESSAGE_PREFIX="${THIS_FN}:"
declare ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
declare VERSION_N='0.1'
declare VERSION_STR="${THIS_FN} version=${VERSION_N}"

### -----------------------------------------------------------------------
### functions
### -----------------------------------------------------------------------

### Draw a Unicode box around a string represented by an AR (bash array representation).
### Input: AR of unboxed string from stdin (pipe or redirect)
### Output: boxed-string AR to stdout
### Dependencies: functions in ./functions_for_lines_to-from_ARs.sh
### TODO: check problem with some-but-not-all Unicode syntax in bash versions >= 4.3
function draw_box_around_text_in_AR() {
    ## raw arg
    local -r IN_AR="${1}" # can quote, since ARs are single strings (TODO: per who?)

    ## note local copies of readonly globals are forbidden in some bash versions :-(
    local -r FN_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${FUNCNAME[0]}:"
    local -r FN_DEBUG_PREFIX="${FN_MESSAGE_PREFIX} DEBUG:"
    local -r FN_ERROR_PREFIX="${FN_MESSAGE_PREFIX} ERROR:"

    ## For "real functionality," make these line-drawing chars configurable.
    ## For Unicode, see https://en.wikipedia.org/wiki/Box-drawing_character
    ## Note: `\u` syntax requires `bash --version` >= 4.2
    ## ... but my current `4.3.30(1)-release (x86_64-pc-linux-gnu)` is only "doing the right thing" with T_CHAR :-(
    ## So have replaced the not-currently-working Unicode syntax with the literal character from https://en.wikipedia.org/wiki/Box-drawing_character#Unicode
    local -r T_CHAR='\u2500'     # top
    local -r B_CHAR="${T_CHAR}"  # bottom
#    local -r L_CHAR='\u2502'     # left
    local -r L_CHAR='│'
    local -r R_CHAR="${L_CHAR}"  # right
#    local -r ULC='\u250C'        # upper-left corner
    local -r ULC='┌'
#    local -r URC='\u2510'        # upper-right corner
    local -r URC='┐'
#    local -r LLC='\u2514'        # lower-left corner
    local -r LLC='└'
#    local -r LRC='\u2518'        # lower-right corner
    local -r LRC='┘'

    ## scratch vars
    local ret_val=''
    local elem_str=''
    local -i counter=0
    local -a out_arr=()          # build up to output boxed string
    local TOP_STRING=''          # intent is read-only (assign-once)
    local BOT_STRING=''          # ditto
    local mid_string=''          # these will change with each line of input
    local mid_string_internal='' # see note below regarding right-padding

    if [[ -z "${IN_AR}" ]] ; then
        >&2 echo "${FN_ERROR_PREFIX} null_AR, exiting ..."
        ## TODO: [discover, add] simple checks for AR validity
        exit 3
    fi
    ## else

    ## describing IN_ARR (which
    ## * intent is read-only
    ## * cannot be previously `declare`d or `local`ed)
    eval "local -a IN_ARR=${IN_AR}"
    if [[ -z "${IN_ARR}" ]] ; then
        >&2 echo "${FN_ERROR_PREFIX} IN_ARR, exiting ..."
        exit 4
    fi
    ## else
#    >&2 echo "${FN_DEBUG_PREFIX} IN_ARR='"
#    for (( counter = 0 ; counter < ${#IN_ARR[@]} ; counter++ )) ; do
#        >&2 echo $(printf '[%s]\t%s\n' "${counter}" "${IN_ARR[${counter}]}")
#    done
#    >&2 echo "' # end IN_ARR"
#    >&2 echo # newline

    # IN_ARR params
    local -i IN_ARR_LEN=0                # ditto
    local -i IN_ARR_LONGEST_ELEM_LEN=0   # ditto

    ## out_arr params
    local -i OUT_ARR_LEN=0               # ditto
    local -i OUT_ARR_LONGEST_ELEM_LEN=0  # ditto

    ## Get sizes of input array, for use in constructing output array.
    ## Yes, we could do this using IN_ARR above, but this gives us yet another opportunity to illustrate a function with AR input/output.
    ## `get_array_parameters` also in `source`d file.

    local IN_ARR_PARAMS_AR="$(get_array_parameters "${IN_AR}")" # respect intent
#    >&2 echo "${FN_DEBUG_PREFIX} input params as AR='${IN_ARR_PARAMS_AR}'"
    if   [[ -z "${IN_ARR_PARAMS_AR}" ]] ; then
        >&2 echo "${FN_ERROR_PREFIX} got null IN_ARR_PARAMS_AR, exiting ..."
        exit 5
    else
        eval "local -a IN_ARR_PARAMS_ARR=${IN_ARR_PARAMS_AR}"
        ## see get_array_parameters::doc in `source`d file
        if (( ${#IN_ARR_PARAMS_ARR[@]} != 2 )) ; then
            >&2 echo "${FN_ERROR_PREFIX} |IN_ARR_PARAMS_ARR| != 2, exiting ..."
            exit 6
        else
            ## see get_array_parameters::doc in `source`d file
            ## TODO: test values received
            (( IN_ARR_LEN=IN_ARR_PARAMS_ARR[0] ))
            (( IN_ARR_LONGEST_ELEM_LEN=IN_ARR_PARAMS_ARR[1] ))
            ## length of IN_ARR, += 2 (for line=T_CHAR (plus corners), line=B_CHAR (ditto))
            (( OUT_ARR_LEN = IN_ARR_LEN + 2 ))
            ## length of longest line in IN_ARR, += 4 (for L_CHAR, SPC, SPC, R_CHAR)
            (( OUT_ARR_LONGEST_ELEM_LEN =  IN_ARR_LONGEST_ELEM_LEN + 4 ))
        fi
    fi

#    >&2 echo "${FN_DEBUG_PREFIX} IN_ARR_LEN='${IN_ARR_LEN}'"
#    >&2 echo "${FN_DEBUG_PREFIX} OUT_ARR_LEN='${OUT_ARR_LEN}'"
#    >&2 echo "${FN_DEBUG_PREFIX} IN_ARR_LONGEST_ELEM_LEN='${IN_ARR_LONGEST_ELEM_LEN}'"
#    >&2 echo "${FN_DEBUG_PREFIX} OUT_ARR_LONGEST_ELEM_LEN='${OUT_ARR_LONGEST_ELEM_LEN}'"

    ## output ~=
    ##     top,bot strings=[left corner, horiz-border chars, right corner]
    ##     each middle string ~= [left-border char, space, line contents, space(s), right-border char]
    ##     middle strings are trickier: gotta right-pad with spaces!-----------^^^

    local -ri HORIZ_BORDER_CHAR_LEN=$(( OUT_ARR_LONGEST_ELEM_LEN - 2 ))
    ## For printing repeating chars, thanks https://stackoverflow.com/a/5349842/915044
    ## (esp note regarding failure of variables within brace expansion).
    ## Note that you should NOT quote the call to `seq`, though `shellcheck` will tell you to!
    TOP_STRING="${ULC}$(printf "${T_CHAR}%.0s" $(seq 1 ${HORIZ_BORDER_CHAR_LEN}))${URC}"
    BOT_STRING="${LLC}$(printf "${B_CHAR}%.0s" $(seq 1 ${HORIZ_BORDER_CHAR_LEN}))${LRC}"
    out_arr[0]="${TOP_STRING}"
    ## ... because 0-based-index(last middle line)==length(input)
    for (( counter = 1 ; counter <= IN_ARR_LEN ; counter++ )) ; do
        mid_string_internal="$(printf "%-${IN_ARR_LONGEST_ELEM_LEN}s" "${IN_ARR[(( counter-1 ))]}")"
        mid_string="${L_CHAR} ${mid_string_internal} ${R_CHAR}"
        out_arr[${counter}]="${mid_string}"
    done
    ## `counter` is still defined outside of loop
    out_arr[${counter}]="${BOT_STRING}"

#    >&2 echo "${FN_DEBUG_PREFIX} out_arr='"
#    for (( counter = 0 ; counter < ${#out_arr[@]} ; counter++ )) ; do
#        >&2 echo $(printf '[%s]\t%s\n' "${counter}" "${out_arr[${counter}]}")
#    done
#    >&2 echo "' # end out_arr"
#    >&2 echo # newline

    ## Return out_arr (which must have been previously `declare`d!) as AR via stdout
    declare -p out_arr | sed -e 's/^declare -a [^=]*=//'
    return 0
} # end function draw_box_around_text_in_AR

### The functionality, to be called from payload (after, e.g., scrubbing args)
function main() {
    ## note local copies of readonly globals are forbidden in some bash versions :-(
    local -r FN_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${FUNCNAME[0]}:"
    local -r FN_DEBUG_PREFIX="${FN_MESSAGE_PREFIX} DEBUG:"
    local -r FN_ERROR_PREFIX="${FN_MESSAGE_PREFIX} ERROR:"

    ## scratch vars
    local ret_val=''
    local elem_str=''

    ## Can't store original text to variable? see save_bash_--version_to_variable_is_painful.sh
#     ## intended read-only but not doable below, so please respect intent
#     local ORIG_TEXT=''
    ## filesystem constants (kludge for `bash --version` below)
    local -r PATH_TO_NORMAL_BV_OUTPUT="$(mktemp)"

    ## See much pain in ./save_bash_--version_to_variable_is_painful.sh
    ## OK, try what works in ./roundtrip_lines_to-from_array_representations.sh : use an AR!
#    bash --version | lines_to_AR | AR_to_lines | read ORIG_TEXT # can't even shortcut that
    bash --version > "${PATH_TO_NORMAL_BV_OUTPUT}"
    echo "${FN_MESSAGE_PREFIX} original text='"
    cat "${PATH_TO_NORMAL_BV_OUTPUT}"
    echo "' # end original text"
    echo # newline

    local -r IN_AR="$(lines_to_AR < "${PATH_TO_NORMAL_BV_OUTPUT}" )" # `source`d function
#    >&2 echo "${FN_DEBUG_PREFIX} about to call function='draw_box_around_text_in_AR' with input as AR='"
#    >&2 echo "${IN_AR}"
#    >&2 echo "' # end input AR"
#    >&2 echo # newline
    local -r OUT_AR="$(draw_box_around_text_in_AR "${IN_AR}")"
    ret_val="${?}"
    if [[ "${ret_val}" -ne 0 ]] ; then
        >&2 echo "${FN_ERROR_PREFIX} ret_val='${ret_val}', exiting ..."
        exit 7
    fi

    ## else, show it
#    >&2 echo "${FN_DEBUG_PREFIX} function='draw_box_around_text_in_AR' returned AR='"
#    >&2 echo "${OUT_AR}"
#    >&2 echo "' # end returned AR"
#    >&2 echo # newline

    eval "local -a out_arr=${OUT_AR}"
    echo "${FN_MESSAGE_PREFIX} prettified text='"
    for elem_str in "${out_arr[@]}" ; do
        echo "${elem_str}"
    done
    >&2 echo "' # end prettified text"
    >&2 echo # newline

    return 0
} # end function main

### ------------------------------------------------------------------
### `source` fence
### ------------------------------------------------------------------

### I.e., don't run any following lines (to EOF) if this file is `source`d

if [[ "${BASH_SOURCE}" != "${0}" ]] ; then
    return 0
fi

### -----------------------------------------------------------------------
### payload
### -----------------------------------------------------------------------

### Functionality code goes here.
### Call function=main (if there are any functions) per Shell Style Guide.
### Reads args from pipe or commandline.

main

### ------------------------------------------------------------------
### execution fence
### ------------------------------------------------------------------

## I.e., don't run any following lines (to EOF), period.

if [[ "${BASH_SOURCE}" == "${0}" ]] ; then
    # ... if this file is executed
    exit 0
else
    # ... if this file is `source`d
    return 0
fi

### ------------------------------------------------------------------
### testcases
### ------------------------------------------------------------------

## Discuss

## * how to test

## * when/where last tested

## -------------------------------------------------------------------
## executable
## -------------------------------------------------------------------

## goto ./%%%__tests.sh

## -------------------------------------------------------------------
## examples
## -------------------------------------------------------------------

### Just run this file:

### 1. replace '/path/to/repo' in `REPO_DIR` with the actual path in your filespace
### 2. if you changed the name of this file (not recommended): change `THIS_FN` appropriately
### 3. cut'n'paste edited code into `bash` console/terminal:

REPO_DIR='/path/to/repo/bash_pass_arrays_between_functions'
THIS_FN='pass_ARs_between_functions.sh'
THIS_FP="${REPO_DIR}/${THIS_FN}"

(( i=0 ))
echo # newline
echo "TEST ${i}: tester='${THIS_FN}' called with FQ path @ $(date)"
echo # newline

pushd /tmp > /dev/null
"${THIS_FP}"
popd > /dev/null

(( i++ ))
echo # newline
echo "TEST ${i}: tester='${THIS_FN}' called with relative path @ $(date)"
echo # newline

pushd "${REPO_DIR}/" > /dev/null
"./${THIS_FN}"
popd > /dev/null

### None of these tests should throw any errors, and output from both tests should resemble:

# > pass_arrays_between_functions.sh::main: original text='
# > GNU bash, version 4.3.30(1)-release (x86_64-pc-linux-gnu)
# > Copyright (C) 2013 Free Software Foundation, Inc.
# > License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html>

# > This is free software; you are free to change and redistribute it.
# > There is NO WARRANTY, to the extent permitted by law.
# > ' # end original text

# > pass_arrays_between_functions.sh::main: prettified text='
# > ┌───────────────────────────────────────────────────────────────────────────────┐
# > │ GNU bash, version 4.3.30(1)-release (x86_64-pc-linux-gnu)                     │
# > │ Copyright (C) 2013 Free Software Foundation, Inc.                             │
# > │ License GPLv3+: GNU GPL version 3 or later <http://gnu.org/licenses/gpl.html> │
# > │                                                                               │
# > │ This is free software; you are free to change and redistribute it.            │
# > │ There is NO WARRANTY, to the extent permitted by law.                         │
# > └───────────────────────────────────────────────────────────────────────────────┘
# > ' # end prettified text
