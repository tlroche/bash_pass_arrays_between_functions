#!/usr/bin/env bash

### USAGE: /path/to/roundtrip_lines_to-from_array_representations.sh
### Path can be FQ or relative.

### Illustrating
### * how to convert arbitrary (and fairly complex) text lines into a `bash` "array representation" (AR)
### * how to convert an AR into text lines
### * that the complete transform (lines -> AR -> lines) is identical with the original input
### Note this was initially inspired by my https://stackoverflow.com/a/39502863/915044
### (which has my acknowledgements to 2nd-order inspirations)
### then further encouraged by difficulties with https://bitbucket.org/tlroche/backup_utils/

### Following attempts to observe Google Shell Style Guide currently @ https://google.github.io/styleguide/shell.xml

### TODO:
### * functions={lines_to_AR , AR_to_lines}: take data via args, not just (as current) pipe || redirect (or create new functions)
### * function=main: take data via args. (Currently, `main` is entirely hard-coded.)
### * (always) run `shellcheck` pre-commit!

### Copyright (C) 2019 Tom Roche <Tom_Roche@pobox.com>
### This work is licensed under the Creative Commons Attribution 4.0 International License.
### To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
### Latest version of this file (or pointer to same) should be available @
### http://bitbucket.org/tlroche/bash_pass_arrays_between_functions/src/master/roundtrip_lines_to-from_array_representations.sh

### ------------------------------------------------------------------
### constants (and very slight init code)
### ------------------------------------------------------------------

## Filesystem constants
## Ordinarily would declare all my `Messaging constants` here,
## but then `source`ing overrides their constants. (TODO: find cleaner way)
## Don't declare following `-r`: more below.

# FQ path to our $(pwd)
declare THIS_DIR="$(readlink -f "$(dirname "${BASH_SOURCE}")")"

## Dependencies

declare -r TRANSFORM_FUNCTIONS_FN='functions_for_lines_to-from_ARs.sh'
declare -r TRANSFORM_FUNCTIONS_FP="${THIS_DIR}/${TRANSFORM_FUNCTIONS_FN}"

### -----------------------------------------------------------------------
### code
### -----------------------------------------------------------------------

### -----------------------------------------------------------------------
### init code (plus more constants)
### -----------------------------------------------------------------------

source "${TRANSFORM_FUNCTIONS_FP}"
## TODO: test return value, errorcode, etc

## Messaging constants
## Don't declare following `-r`: same names are used in other files in this project.
## (TODO: find how to reconcile both read-only declarations, which is my intent.)

declare THIS_FP="${BASH_SOURCE}" # works when either executing or `source`ing
declare THIS_FN="$(basename "${THIS_FP}")"
# declare THIS_DIR above
declare MESSAGE_PREFIX="${THIS_FN}:"
declare ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
declare VERSION_N='0.1'
declare VERSION_STR="${THIS_FN} version=${VERSION_N}"

### -----------------------------------------------------------------------
### functions
### -----------------------------------------------------------------------

### Main functionality, to be called from section=payload.
### Since so much of this functionality relies on `stdout`,
### DON'T message or debug there! Instead use `stderr` (i.e., `>&2`)
function main() {
    ## note local copies of readonly globals are forbidden in some bash versions :-(
    local -r FN_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${FUNCNAME[0]}:"
    local -r FN_DEBUG_PREFIX="${FN_MESSAGE_PREFIX} DEBUG:"
    local -r FN_ERROR_PREFIX="${FN_MESSAGE_PREFIX} ERROR:"
    ## filesystem constants: storage for various outputs
    local -r PATH_TO_NORMAL_BV_OUTPUT="$(mktemp)"
    local -r PATH_TO_BV_AR="$(mktemp)"
    local -r PATH_TO_L2AR2L_BV_OUTPUT="$(mktemp)"
    ## scratch vars
    local ret_val=''
    local elem_str=''

    >&2 echo "${FN_MESSAGE_PREFIX} Capture normal \`bash --version\` output normally @ '${PATH_TO_NORMAL_BV_OUTPUT}'"
    bash --version > "${PATH_TO_NORMAL_BV_OUTPUT}"
    >&2 echo "'${PATH_TO_NORMAL_BV_OUTPUT}' listing:"
    ## Note: `ls -gG` -> don't list {owner, group}. Convenience for copying output to doc.
    >&2 ls -algG "${PATH_TO_NORMAL_BV_OUTPUT}"
    >&2 echo "'${PATH_TO_NORMAL_BV_OUTPUT}' content (fake line#s via \`nl\`):"
    >&2 nl --body-numbering=a "${PATH_TO_NORMAL_BV_OUTPUT}"
    >&2 echo # newline

    >&2 echo "${FN_MESSAGE_PREFIX} Read \`bash --version\` {into, out from) array representation (AR)."
    ## Not as easy :-) 1st, ...
    bash --version | lines_to_AR > "${PATH_TO_BV_AR}"
    ## ... which is an array *representation* (AR), NOT an array!
    >&2 echo "Should you care to inspect it, its AR is captured @ '${PATH_TO_BV_AR}'"
    >&2 echo "'${PATH_TO_BV_AR}' listing:"
    >&2 ls -algG "${PATH_TO_BV_AR}"
    >&2 echo # newline

    echo "${FN_MESSAGE_PREFIX} Completely-transformed (lines -> AR -> lines) output @ '${PATH_TO_L2AR2L_BV_OUTPUT}'"
    AR_to_lines < "${PATH_TO_BV_AR}" > "${PATH_TO_L2AR2L_BV_OUTPUT}"
    ## Note we can also do this (and quoting `echo` argument is OK) ...
#    echo "${L2AR_OF_BASH_VERSION}" | AR_to_lines > "${PATH_TO_L2AR2L_BV_OUTPUT}"
    ## ... because ARs are guaranteed (by who?) to be single lines.
    >&2 echo "'${PATH_TO_L2AR2L_BV_OUTPUT}' listing:"
    >&2 ls -algG "${PATH_TO_L2AR2L_BV_OUTPUT}"
    >&2 echo "'${PATH_TO_L2AR2L_BV_OUTPUT}' content (fake line#s via \`nl\`):"
    >&2 nl --body-numbering=a "${PATH_TO_L2AR2L_BV_OUTPUT}"
    >&2 echo # newline

    >&2 echo "${FN_MESSAGE_PREFIX} Is the complete-transform (L2AR2L) output identical to the old-fashioned output?"
    >&2 echo "Per \`diff -uwB ${PATH_TO_NORMAL_BV_OUTPUT} ${PATH_TO_L2AR2L_BV_OUTPUT} | wc -l\` , yes (as long as the following output=='0'):"
    # off stderr for convenience
    diff -uwB "${PATH_TO_NORMAL_BV_OUTPUT}" "${PATH_TO_L2AR2L_BV_OUTPUT}" | wc -l

} # end function main

### ------------------------------------------------------------------
### `source` fence
### ------------------------------------------------------------------

### I.e., don't run any following lines (to EOF) if this file is `source`d

if [[ "${BASH_SOURCE}" != "${0}" ]] ; then
    return 0
fi

### -----------------------------------------------------------------------
### payload
### -----------------------------------------------------------------------

### Functionality code goes here.

### TODO: take arguments!
main

### ------------------------------------------------------------------
### execution fence
### ------------------------------------------------------------------

## I.e., don't run any following lines (to EOF), period.

if [[ "${BASH_SOURCE}" == "${0}" ]] ; then
    # ... if this file is executed
    exit 0
else
    # ... if this file is `source`d
    return 0
fi

### ------------------------------------------------------------------
### testcases
### ------------------------------------------------------------------

## Discuss

## * how to test

## * when/where last tested

## -------------------------------------------------------------------
## executable
## -------------------------------------------------------------------

## goto ./%%%__tests.sh

## -------------------------------------------------------------------
## examples
## -------------------------------------------------------------------

### Just run this file:

### 1. replace '/path/to/repo' in `REPO_DIR` with the actual path in your filespace
### 2. if you changed the name of this file (not recommended): change `THIS_FN` appropriately
### 3. cut'n'paste edited code into `bash` console/terminal:

REPO_DIR='/path/to/repo/bash_pass_arrays_between_functions'
THIS_FN='roundtrip_lines_to-from_array_representations.sh'
THIS_FP="${REPO_DIR}/${THIS_FN}"

(( i=0 ))
echo # newline
echo "TEST ${i}: tester='${THIS_FN}' called with FQ path @ $(date)"
echo # newline

pushd /tmp > /dev/null
"${THIS_FP}"
popd > /dev/null

(( i++ ))
echo # newline
echo "TEST ${i}: tester='${THIS_FN}' called with relative path @ $(date)"
echo # newline

pushd "${REPO_DIR}/" > /dev/null
"./${THIS_FN}"
popd > /dev/null

### Output from both tests should look very similar,
### (particularly the `diff` at end of both should == 0), and
### neither should throw any errors.
