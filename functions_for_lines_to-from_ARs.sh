#!/usr/bin/env bash

### Functions supporting transforming (text) lines <-> `bash` "array representations" (ARs).
### Usage: `source` this file, call functions.

### Following attempts to observe Google Shell Style Guide currently @ https://google.github.io/styleguide/shell.xml

### TODO:
### * functions={lines_to_AR , AR_to_lines}: take data via args, not just (as current) pipe || redirect. (Or create new functions.)
### * (always) run `shellcheck` pre-commit!

### Copyright (C) 2019 Tom Roche <Tom_Roche@pobox.com>
### This work is licensed under the Creative Commons Attribution 4.0 International License.
### To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/
### Latest version of this file (or pointer to same) should be available @
### http://bitbucket.org/tlroche/bash_pass_arrays_between_functions/src/master/pass_arrays_between_functions.sh

### ------------------------------------------------------------------
### constants
### ------------------------------------------------------------------

## Messaging constants
## Don't declare following `-r`: same names are used in other files in this project.
## (TODO: find how to reconcile both read-only declarations, which is my intent.)

declare THIS_FP="${BASH_SOURCE}" # works when either executing or `source`ing
declare THIS_FN="$(basename "${THIS_FP}")"
declare THIS_DIR="$(readlink -f "$(dirname "${THIS_FP}")")"  # FQ path to caller's $(pwd)
declare MESSAGE_PREFIX="${THIS_FN}:"
declare ERROR_PREFIX="${MESSAGE_PREFIX} ERROR:"
declare VERSION_N='0.1'
declare VERSION_STR="${THIS_FN} version=${VERSION_N}"

### ------------------------------------------------------------------
### Notes on AR <-> array conversions
### ------------------------------------------------------------------

### There are no functions `array_to_AR` or `AR_to_array` precisely because
### one cannot natively pass an array to or from a function in `bash`--
### working around this problem is precisely our motivation.
### However, one can do these conversions with the following simple 'human macros':
### remember (or cut'n'paste :-) the following, and substitute where needed:

### array -> AR: e.g.,

### > declare -p OUT_ARRAY | sed -e 's/^declare -a [^=]*=//'

### This 1liner outputs an AR via stdout. Note that:

### * `OUT_ARRAY` must already be populated, and therefore should have been `declare`d or `local`ed in prior code. This is because ...
### * `declare -p` does not declare in the usual sense of the term.
###   Per https://www.gnu.org/software/bash/manual/html_node/Bash-Builtins.html (scroll to header=declare),
###   `declare -p name` 'will display the attributes and values of each name. When -p is used with name arguments, additional options, other than -f and -F, are ignored.'

### AR -> array:

### > eval "local -a in_array=${IN_AR}"

### This 1liner takes as input the previously-created `IN_AR`, and
### outputs to 'in_array', which must NOT
### - be here declared `-ar`||`-ra` : read-only gets syntax error=`not a valid identifier`
### - be previously `declare`d or `local`ed

### Similarly, note that

### * `bash` arrays are inherently one-dimensional (though one can simulating multi-dimensional arrays with code:
###   see, e.g., 'Example 27-17' in the (excellent and extensive) Advanced Bash-Scripting Guide @
###   http://www.tldp.org/LDP/abs/html/arrays.html#TWODIM
###   ) and therefore ...

### * One can simply {dump, list, print} an array's contents with the following 1liner
###   (though you should probably format the loop more appropriately in your code):
### > for str in "${array[@]}" ; do echo "${str}" ; done

### * To {dump, list, print} an array's contents:
###   one way: plain, no decorations. This is both simpler and
###   the way ya gotta do it if returning array contents via stdout
### > for elem_str in "${out_array[@]}" ; do
### >     echo "${elem_str}"
### > done
###
###   another, prettier:
### > for (( index = 0 ; index < ${#array[@]} ; index++ )) ; do
### >     echo $(printf "[%s]\t%s\n" "${index}" "${array[${index}]}")
### > done

### -----------------------------------------------------------------------
### code
### -----------------------------------------------------------------------

## functions only

### -----------------------------------------------------------------------
### functions
### -----------------------------------------------------------------------

### Use bash built-in 'readarray' (aka 'mapfile') to read
###  input: lines of text from stdout (via pipe or redirect)
### output: array representation (AR) to stdout
### TODO: read from argument=filepath.
### (For RTFM, see https://www.gnu.org/software/bash/manual/html_node/Bash-Builtins.html
### but ya gotta scroll/search to header='mapfile'--no anchors :-(
### Handles blank lines, whitespace, and nasty/reserved characters.

### `bash --version` dependency: TODO: (IIRC, >= 4.3, but ICBW)
### Non-bash dependencies: sed
function lines_to_AR {
    local -a arr=()

    ## mapfile/readarray "reads lines from [input stream] into the indexed array variable."
    ## `-t` == 'Remove a trailing delim (default newline) from each line read.'
    ## (for `bash --version` >= 4.4?) delim can be set with `-d` (default=`\n`)
    ## (for which `bash --version` ?) stream can be set with `-u` (default=stdout)
    ## Since this is the 1st command in the function, it reads from the pipe.

    # Note also: DO NOT quote 'arr' in the following!
    readarray -t arr
    # output array as string/AR using `declare -p` (which is not declaring in the usual sense of the term)
    declare -p arr | sed -e 's/^declare -a [^=]*=//'
} # end function lines_to_AR

### The inverse of function=lines_to_AR (above).
### Input: single AR line, either via argument or (stdout) pipe/redirect.
### ASSERT: AR must be valid. (TODO: discover how to self-validate AR strings.)
### Output: lines of text (to stdout)

### `bash --version` dependency: TODO (IIRC, >= 4.3, but ICBW)
function AR_to_lines {
    local -ri N_ARGS=${#}
    local IN_AR=''         # I'd like to make the AR read-only, but can't; respect intent!
#    local -a out_array=() # can't do here, do below
    local elem_str=''
    local -r FN_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${FUNCNAME[0]}:"
    local -r FN_ERROR_PREFIX="${FN_MESSAGE_PREFIX} ERROR:"

    if   (( N_ARGS == 0 )) ; then # zero commandline args à la pipe/redirect
        read IN_AR                # from stdin
    elif (( N_ARGS == 1 )) ; then # 1 commandline arg
        IN_AR="${1}"
    else
        >&2 echo "${FN_ERROR_PREFIX} cannot handle |commandline args| == ${#}"
        return 3
    fi

    ## Convert AR to array, ...
    eval "local -a out_array=${IN_AR}"
    ## ... then dump the array to stdout.
    for elem_str in "${out_array[@]}" ; do
        echo "${elem_str}"
    done
} # end function AR_to_lines

### Get parameters for array:
###  input: array as AR
### output: AR of input [array length, length of longest element (as line of text)]

### `bash --version` dependency: TODO: (IIRC, >= 4.3, but ICBW)
### Non-bash dependencies: sed
function get_array_parameters {
    local -ri N_ARGS=${#}
    local -r FN_MESSAGE_PREFIX="${MESSAGE_PREFIX}:${FUNCNAME[0]}:"
    local -r FN_ERROR_PREFIX="${FN_MESSAGE_PREFIX} ERROR:"

    local IN_AR=()      # I'd like to make the AR read-only, but can't; respect intent!
    local elem_str=''
    local -i counter

    local -i IN_ARR_LEN=0                # intent read-only
    local -i IN_ARR_LONGEST_ELEM_LEN=0   # ditto

    if   (( N_ARGS == 0 )) ; then # zero commandline args à la pipe/redirect
        read IN_AR                # from stdin
    elif (( N_ARGS == 1 )) ; then # from single commandline arg
        IN_AR="${1}"
    else
        >&2 echo "${FN_ERROR_PREFIX} cannot handle |commandline args| == ${#}"
        return 3
    fi

    ## Convert AR to array, ...
    eval "local -a IN_ARR=${IN_AR}" # cannot `declare`||`local` previously, cannot `-r` here
    ## ... and get parameters
    (( IN_ARR_LEN=${#IN_ARR[@]} ))
    for (( counter = 0 ; counter < IN_ARR_LEN ; counter++ )) ; do
        if (( ${#IN_ARR[${counter}]} > IN_ARR_LONGEST_ELEM_LEN )) ; then
            (( IN_ARR_LONGEST_ELEM_LEN=${#IN_ARR[${counter}]} ))
        fi
    done
    ## TODO: test parameters!

    ## create output array ...
    # Note: `-ar`||`-ra` -> error: `not a valid identifier`
#    local -a OUT_ARR=( ${IN_ARR_LEN} , ${IN_ARR_LONGEST_ELEM_LEN} )
# comma becomes element[1] !
    local -a OUT_ARR=( ${IN_ARR_LEN} ${IN_ARR_LONGEST_ELEM_LEN} )
    ## ... and output AR via stdout
    declare -p OUT_ARR | sed -e 's/^declare -a [^=]*=//'
} # end function get_array_parameters

### ------------------------------------------------------------------
### `source` fence
### ------------------------------------------------------------------

### I.e., don't run any following lines (to EOF) if this file is `source`d

if [[ "${BASH_SOURCE}" != "${0}" ]] ; then
    return 0
fi

### ------------------------------------------------------------------
### testcases
### ------------------------------------------------------------------

## Discuss

## * how to test

## * when/where last tested

## -------------------------------------------------------------------
## executable
## -------------------------------------------------------------------

## goto ./%%%__tests.sh

## -------------------------------------------------------------------
## examples
## -------------------------------------------------------------------

### Use simple scripts contained in this repo='bash_pass_arrays_between_functions' to test above functions. Just

### 1. replace '/path/to/repo' in `REPO_DIR` with the actual path in your filespace
### 2. if you changed the names of files in this repo (not recommended): change `TESTER_FN_ARR` appropriately
### 3. cut'n'paste edited code into `bash` console/terminal:

REPO_DIR='/path/to/repo/bash_pass_arrays_between_functions'
TESTER_FN_ARR=( 'roundtrip_lines_to-from_array_representations.sh' 'pass_ARs_between_functions.sh' )

for (( i=0 ; i < ${#TESTER_FN_ARR[@]} ; i++ )) ; do
    TESTER_FN="${TESTER_FN_ARR[${i}]}"
    TESTER_FP="${REPO_DIR}/${TESTER_FN}"

    (( j=0 ))
    echo # newline
    echo "TEST ${i}.${j}: tester='${TESTER_FN}' called with FQ path @ $(date)"
    echo # newline

    pushd /tmp > /dev/null
    "${TESTER_FP}"
    popd > /dev/null

    (( j++ ))
    echo # newline
    echo "TEST ${i}.${j}: tester='${TESTER_FN}' called with relative path @ $(date)"
    echo # newline

    # test i.1: use relpath
    pushd "${REPO_DIR}/" > /dev/null
    "./${TESTER_FN}"
    popd > /dev/null
done

### Output from each pair of tests should look very similar,
### (particularly the `diff` at end of both should == 0), and
### neither should throw any errors.
